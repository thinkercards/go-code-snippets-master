package main
import "fmt"
import "os"
import "errors"

//function accepts a filename and tries to open it.
func fileopen(name string) (string, error,string) {
    f, er := os.Open(name)

    //er will be nil if the file exists else it returns an error object  
    if er != nil {


        //created a new error object and returns it  
        return "", errors.New("Custom error message: File name is wrong for user"),"error test value"
    }else{
    	return f.Name(),nil,"test file open"
    }
}

func main() {  
    //receives custom error or nil after trying to open the file
    filename, error,result := fileopen("test.txt")
    if error != nil {
        fmt.Println(error)
    }else{
    	fmt.Println("file opened", filename)
    }  

	fmt.Println(result)


}